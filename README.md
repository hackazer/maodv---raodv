# MAODV - RAODV
**Paper Implementation on NS2  - A Modified AODV Protocol Based on Nodes Velocity**

**By :**

     -  Rizaldy Primanta Putra
     -  5115100046
     -  Kelas JARNIL 2018

---
Konsep : <br>
Modifikasi AODV menjadi RAODV. Disini AODV tetap akan bekerja seperti aodv pada umumnya, hanya saja terjadi sedikit perubahan pada **RREQ** dan **RREP**. 
Yakni, terdapat penambahan parameter (variabel) baru yakni :
 - *V* (velocity/kecepatan) 
 - *lcf_sum* (total link stability coefficient)

	>Rumus :
	**lcf** (link stability coefficient) = **(Vi - Vy) * (Vi - Vy)**

Pada saat fase RREQ, <br>
**Pertama** source node akan menambahkan **V / kecepatan** pada node **ke-i** (dimulai dari node source), ***hop_count***, ***dan lcf_sum*** = 0. <br>
**Kedua** RREQ akan dibroadcast terhadap neighbor node. Ketika paket telah diterima,  neighbor node tersebut akan menghitung jumlah ***lcf_sum*** (sesuai rumus), jumlah ***hop_count***, dan mengupdate nilai ***V*** dengan kecepatan node tersebut. <br>
**Ketiga**, packet RREQ ini akan terus dibroadcast dengan cara yang sama seperti diatas hingga ke destination node.<br>
**Keempat**, destination node akan memilih routing path berdasar : 
 - Jika, total ***hop_count*** > 10, maka akan diambil path dengan nilai
   ***lcf_sum*** (link stability coefficient) terkecil
 - Jika total ***hop_count*** < 10, makan akan diambil path dengan total
   ***hop_count*** terkecil
   
   ---

# Working on Concept

Kali ini kita akan menggunakan fungsi yang telah di define di **NS2**, yakni : <br><br>

- Modifikasi pada header aodv (aodv.h)<br>
![enter image description here](https://bypass.id/img/aodvh1.PNG)

<br><br>Pada bagian diatas ditambahkan sebuah class struct baru untuk menyimpan nilai speed dan link coefficient dari node agar bisa dikirimkan. <br>
Kemudian menambahkan field pada class AODV bagian public agent agar bisa digunakan sebagai constructor di file aodv.cc nanti. <br><br>

![enter image description here](https://bypass.id/img/aodvh2.PNG)
<br><br>

X,y,z pos merupakan varibael untuk mendefinisikan posisi node pada bidang x,y,z. <br>
iNode merupakan variable turunan dari class MobileNode yang digunakan untuk mendapatkan speed. <br><br>

- Modifikasi constructor untuk mendefinisikan kecepatan, counter, link coefficient, counter, kecepatan (aodv.cc)<br><br>
![enter image description here](https://bypass.id/img/aodvcc1.PNG)
<br><br>

Pada hal ini mendefinisikan pada bagian constructor AODV (AODV::AODV(nsaddr_t id) : Agent(PT_AODV),xxx) <br<br>

**dest_linkoef** merupakan variable sementara untuk menyimpan nilai **link coefficient** pada node destinasi, <br>
sehingga untuk dibandingkan dengan jumlah link coefficient yang masuk pada node destinasi. <br><br>

- Modifikasi pada sendReq (aodv.cc)
<br><br>

![enter image description here](https://bypass.id/img/aodvcc2.PNG)
<br><br>

Pada saat index == 8 disini ialah node source (dalam kasus ini node sourceialah 8, dan destinasi ialah 9). <br>
Pada saat di index 8, atau node pertama, mendefinisikan kecepatan, **link coefficient** menjadi 0. <bR><br>

- Modifikasi pada RecvReq (aodv.cc)
<br><br>
![enter image description here](https://bypass.id/img/aodvrecvReq1.PNG)
<br><br>

Pada bagian diatas, pada fungsi recvRequest, kita mendefinisikan nilai kecepatan untuk dimasukan kedalam class struct yang telah dibuat tadi pada file aodv.h. <br>
Kemudian memasukan rumus untuk menghitung link coefficient, dan memasukan hasil tersebut kedalam struct class tadi. <br>
Lalu dilanjut dengan mengedit bagian recvRequest pada section dimana kita mengecek apakah node itu destinasi / bukan. <br>
Kemudian dengan memasukan counter dan membuat nilai dest_linkcoef agar dibandingkan dengan nilai jumlah linkcoef dari node node sebelumnya. <br>
Dan dilakukan pengecekan pula, apakah hop count kurang dari 8 atau tidak, jika tidak maka akan dilakukan pengecekan berupa ranking dengan nilai link coefficient terkecil.<br><br>

![enter image description here](https://bypass.id/img/aodvrecvReq2.PNG)
<br>
![enter image description here](https://bypass.id/img/aodvrecvReq3.PNG)
